﻿<?xml version="1.0"?>
<!DOCTYPE rfc SYSTEM "rfc2629.dtd">
<?rfc toc="yes"?>
<?rfc comments="yes"?>
<?rfc inline="yes"?>
<?rfc subcompact="yes"?>
<rfc category="std" ipr="trust200902" docName="draft-duquennoy-6tisch-asf-01">
<front>
    <title>
        6TiSCH Autonomous Scheduling Function (ASF)
    </title>
    <author initials="S" surname="Duquennoy" fullname="Simon Duquennoy" role="editor">
        <organization>RISE SICS</organization>
        <address>
            <postal>
                <street>Isafjordsgatan 22</street>
                <city>164 29 Kista</city>
                <country>Sweden</country>
            </postal>
            <email>simon.duquennoy@ri.se</email>
        </address>
    </author>
    <author initials="X" surname="Vilajosana" fullname="Xavier Vilajosana" >
        <organization>Universitat Oberta de Catalunya</organization>
        <address>
            <postal>
                <street>156 Rambla Poblenou</street>
                <city>Barcelona</city>
                <region>Catalonia</region>
                <code>08018</code>
                <country>Spain</country>
            </postal>
            <email>xvilajosana@uoc.edu</email>
        </address>
    </author>
    <author initials="T" surname="Watteyne" fullname="Thomas Watteyne">
        <organization>Inria</organization>
        <address>
            <postal>
                <street>2 Rue Simone Iff</street>
                <city>Paris</city>
                <country>France</country>
            </postal>
            <email>thomas.watteyne@inria.fr</email>
        </address>
    </author>
    <date/>
    <area>Internet Area</area>
    <workgroup>6TiSCH</workgroup>
    <keyword>Draft</keyword>
    <abstract>
        <t>
            This document defines a Scheduling Function called "ASF": the 6TiSCH Autonomous Scheduling Function.
            With ASF, nodes maintain their TSCH schedule based on local neighborhood knowledge, without any signaling after association.
            Hashes of the nodes' MAC address are used to deterministically derive the [slotOffset,channelOffset] location of cells in the TSCH schedule.
            Different traffic types (e.g. TSCH EB, RPL DIO, UDP etc.) are assigned to distinct slotframes, for isolation and flexible dimensioning.
            This approach provides over-provisioned schedules with low maintenance, in pursuit for simplicity rather than optimality.
        </t>
    </abstract>
    <note title="Requirements Language">
        <t>
            The key words "MUST", "MUST NOT", "REQUIRED", "SHALL", "SHALL NOT", "SHOULD", "SHOULD NOT", "RECOMMENDED", "NOT RECOMMENDED", "MAY", and "OPTIONAL" in this document are to be interpreted as described in <xref target="RFC2119">RFC 2119</xref>.
        </t>
    </note>
</front>
<middle>
    <section title="TEMPORARY EDITORIAL NOTES">
        <t>
            This document is an Internet Draft, so work-in-progress by nature.
            It contains the following work-in-progress elements:
            <list style="symbols">
                <t>
                    "TODO" statements are elements which have not yet been written by the authors for some reason (lack of time, ongoing discussions with no clear consensus, etc).
                    The statement does indicate that the text will be written at some point.
                </t>
                <t>
                    "TEMPORARY" appendices are there to capture current ongoing discussions, or the changelog of the document.
                    These appendices will be removed in the final text.
                </t>
                <t>
                    "IANA_*" identifiers are placeholders for numbers assigned by IANA.
                    These placeholders are to be replaced by the actual values they represent after their assignment by IANA.
                </t>
                <t>
                    "RFCXXXX" refers to the RFC number of this specification, once published.
                </t>
                <t>
                    The string "REMARK" is put before a remark (questions, suggestion, etc) from an author, editor or contributor.
                    These are on-going discussions at the time of writing, and will not be part of the final text.
                </t>
                <t>
                    This section will be removed in the final text.
                </t>
            </list>
        </t>
    </section>
    <section title="Introduction"                                         anchor="sec_intro">
        <t>
            This document defines an autonomous Scheduling Function for the 6top sublayer <xref target="I-D.ietf-6tisch-6top-protocol"/>, called "ASF".
            It is designed to operate without any runtime signaling, keeping the TSCH schedule consistent between neighbors at all times (slots for transmission and reception always match).
            ASF uses 6P solely for configuration at association time (6P SIGNAL) and for schedule inspection (6P STATUS and LIST).
            ASF isolates different traffic types into distinct slotframes, so as to avoid any disruption between MAC synchronization, control and application traffic.
        </t>
        <t>
            ASF addresses all requirements listed in Section "Requirements for an SF" from <xref target="I-D.ietf-6tisch-6top-protocol"/>.
            The organization of this document follows section "Recommended Structure of an SF Specification" in <xref target="I-D.ietf-6tisch-6top-protocol"/>.
            This document follows the terminology defined in <xref target="I-D.ietf-6tisch-terminology"/>.
        </t>
        <section title="Application Domains"                              anchor="sec_applications">
            <t>
                ASF is primarily targeted at applications with random traffic flows, such as interactive CoAP traffic.
                Its main strength is its signaling-free nature, which ensures the slots installed at neighboring nodes are consistent at all times.
                Its main weakness is its contention-based nature and its need to over-provision the schedule, rendering it unable to meet stringent latency and energy requirements.
                An example application domains is building instrumentation.
                ASF was evaluated experimentally and shown to achieve over 99.99% end-to-end delivery in 6TiSCH/RPL testbeds <xref target="Orchestra-SenSys" />.
            </t>
        </section>
    </section>
    <section title="General Operation"                                    anchor="sec_generalop">
        <t>
            ASF uses multiple slotframes, each assigned to one particular type of traffic, e.g. TSCH EBs, RPL or UDP traffic.
            Nodes maintain the cells within the slotframes autonomously, based on the hash of either the source's or destination's MAC address.
            Each slotframe is uniquely assigned a set of channel offsets.
        </t>
        <section title="Cell Coordinates"                                 anchor="sec_cell_coordinates">
            <t>
                Cell coordinates in ASF are either fixed or derived from a MAC address (depending on the slotframe type, see <xref target="sec_types_of_slotframes" />).
                To derive coordinates from a MAC (EUI-64) address, nodes MUST use the hash function provided at configuration time, see <xref target="sec_configparams" />.
                One example hash function is SAX <xref target="SAX-DASFAA"/>.
                Let S_len be the length of slotframe S, and S_channels be the set of channels assigned to slotframe S.
                The slot coordinates derived from a given MAC address are computed as follows:
                <list>
                    <t>slotOffset(MAC) = hash(MAC) % S_len </t>
                    <t>channelOffset(MAC) = S_channels[(hash(MAC) / L) % len(S_channels)]</t>
                </list>
            </t>
        </section>
        <section title="Types of Slotframes"                              anchor="sec_types_of_slotframes">
            <t>
                There are three different types of slotframes, described next.
                <xref target="sec_configparams" /> provides full details on cell options and other aspects.
            </t>
            <section title="Rendez-vous slotframe"                        anchor="sec_slotframes_rdv">
                <t>
                    Contains a single contention-based rendez-vous cell, at coordinates [slot offset: 0; channel offset: 0].
                    This slotframe is equivalent to the 6TiSCH minimal schedule <xref target="RFC8180"/>.
                </t>
            </section>
            <section title="Receiver-based slotframe"                     anchor="sec_slotframes_rb">
                <t>
                    <list hangIndent="3" style="hanging">
                        <t hangText="One Rx cell:">
                            Coordinates computed as the hash of the node's own MAC address.
                        </t>
                        <t hangText="Multiple Tx cells:">
                            One Tx cell per neighbor.
                            Coordinates computed as the hash of the neighbor's MAC address.
                        </t>
                    </list>
                </t>
            </section>
            <section title="Sender-based slotframe"                       anchor="sec_slotframes_sb">
                <t>
                    <list hangIndent="3" style="hanging">
                        <t hangText="One Tx cell:">
                            Coordinates computed as the hash of the node's own MAC address.
                        </t>
                        <t hangText="Multiple Rx cells:">
                            One Rx cell per neighbor.
                            Coordinates computed as the hash of the neighbor's MAC address.
                        </t>
                    </list>
                </t>
            </section>
        </section>
        <section title="Conditional Cells"                                anchor="sec_frame_pending_bit">
            <t>
                In order to handle traffic bursts, ASF utilizes conditional cells.
                When a node has several frames in its queue for a given neighbor, it can set the <xref target="IEEE802154-2015"/> 'frame pending' bit in unicast transmissions to that neighbor.
                Cells at upcoming time offsets will be used to carry more frames.
                Note that collisions may happen on these conditional cells, which MUST therefore have the 'Shared' bit set.
                <list hangIndent="3" style="hanging">
                    <t hangText="Sender:">
                        A sender with multiple unicast frames in its queue for a given neighbor MAY send frames with the 'frame pending' bit set.
                        After sending a unicast frame with the 'frame pending' bit set, if a link-layer Acknowledgment (ACK) is received, the sender immediately schedules a temporary Tx cell.
                        Compared to the initial cell, the temporary cell has
                            the same Link Options plus the 'Shared' bit,
                            the same channel offset, and
                            a time offset incremented by 1 (modulo the slotframe length).
                        The next frame will be sent on this temporary cell, and may set the 'frame pending' bit again to signal more traffic to come.
                        The procedure repeats until the transmit queue is empty, or until no acknowledgment is received for a frame.
                    </t>
                    <t hangText="Receiver:">
                        Upon receiving a unicast frame with the 'frame pending' bit set, the node first sends a link-layer ACK.
                        It then schedules a temporary Rx cell, with same Link Options, same channel offset, and time offset incremented by 1.
                        If, in the new cell, it receives a unicast frame with the 'frame pending' bit set, it continues scheduling additional Rx cells to receive subsequent frames.
                    </t>
                </list>
            </t>
        </section>
        <section title="Interaction between Slotframes"                   anchor="sec_slotframe_interaction">
            <t>
                ASF is expected to maintain multiple slotframes, each dedicated to a different traffic type.
                As the slotframes repeat over time, cells from different slotframes overlap periodically.
                In case a node has multiple cells scheduled at the same time, the precedence rules from <xref target="IEEE802154-2015"/> apply.
                In order to distribute cell overlap uniformly, it is RECOMMENDED to select slotframe lengths that are co-primes.
            </t>
        </section>
    </section>
    <section title="Configuration"                                        anchor="sec_configparams">
        <t>
            An ASF configuration consists of a series of slotframes with attributes.
            ASF uses the 6P SIGNAL command (format in <xref target="tab_asf_signal" />) to disseminate its configuration.
            SIGNAL commands are directly included as IETF IE in each EB, so that nodes learn the ASF configuration directly at join-time.
        </t>
        <t>
            6TiSCH EBs are not secured.
            For applications that require the ASF schedule to be sent securely, the ASF SIGNAL command MAY be sent instead in separate data broadcast packets, after join-time.
            To summarize, there are two cases:
            <list hangIndent="3" style="hanging">
                <t hangText="Initial EB includes ASF SIGNAL:">
                    The node configures itself to run the ASF configuration provided.
                </t>
                <t hangText="Initial EB does not include ASF SIGNAL:">
                    The node runs the 6TiSCH minimal schedule (<xref target="RFC8180"/>) at association.
                    When later receiving a packet with ASF SIGNAL, the node replaces the 6TiSCH minimal schedule with the ASF configuration provided.
                </t>
            </list>
        </t>
        <t>
            <xref target="tab_asf_signal" /> describes the format of the ASF SIGNAL command.
        </t>
        <figure title="Format of the ASF SIGNAL command."                 anchor="tab_asf_signal">
<artwork>
                  1                   2                   3
0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
| # Slotframes  | Slotframe list ...
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
</artwork>
        </figure>
        <t>
            Where:
            <list hangIndent="3" style="hanging">
                <t hangText="# Slotframes:">
                    The number of ASF slotframes
                </t>
                <t hangText="Slotframe list:">
                    The list of slotframes, with format described in <xref target="tab_asf_signal_slotframe" />
                </t>
            </list>
        </t>
        <figure title="Format of the ASF SIGNAL slotframe descriptor."    anchor="tab_asf_signal_slotframe">
<artwork>
                  1                   2                   3
0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
| Slotf. handle |         Slotframe size        |      Type     |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|      Min. channel offset      |      Max. channel offset      |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|  Tx Cell Opt  |  Rx Cell Opt  |    Nbr Set    |   Hash func.  |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|   # Filters   | Traffic filter list ...
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-
</artwork>
        </figure>
        <t>
            <xref target="tab_asf_signal_slotframe" /> shows the format of a slotframe descriptor, where:
            <list hangIndent="3" style="hanging">
                <t hangText="Slotf. handle:">
                    The IEEE 802.15.4 slotframe handle (8 bits)
                </t>
                <t hangText="Slotframe size:">
                    The IEEE 802.15.4 slotframe size (16 bits)
                </t>
                <t hangText="Type:">
                    The ASF slotframe type.
                    The set of possible values for this field is presented in  <xref target="tab_types_of_slotframes"/>.
                    The different slotframe types are described in <xref target="sec_types_of_slotframes"/>.
                </t>
                <t hangText="Min. channel offset:">
                    ASF slotframes are assigned a channel offset range.
                    This defines the lower bound for the range.
                </t>
                <t hangText="Max. channel offset:">
                    ASF slotframes are assigned a channel offset range.
                    This defines the upper bound for the range.
                </t>
                <t hangText="Tx Cell Opt:">
                    The options to be used for the Tx Cells, if any.
                </t>
                <t hangText="Rx Cell Opt:">
                    The options to be used for the Rx Cells, if any.
                </t>
                <t hangText="Nbr Set:">
                    The set of neighbors for which Cells are instantiated.
                    The set of possible values for this field is presented in <xref target="tab_nbr_set"/>.
                </t>
                <t hangText="Hash func.:">
                    The hash function used to compute cell coordinates from a node's EUI-64 address, as defined in <xref target="sec_cell_coordinates"/>.
                    The set of possible values for this field is presented in <xref target="tab_hash_func"/>.
                </t>
                <t hangText="# Filters:">
                    ASF slotframes are assigned a subset of the traffic each.
                    One or several traffic filters will be applied to only select packets with the intended properties.
                    When there are several traffic filters, they are combined with a OR, i.e., packets that satisfy any of the filters will be sent on the slotframe.
                    This field defines how many filters are in place.
                    The filter descriptions follow inline, with format defined in  <xref target="tab_asf_signal_filters"/>.
                </t>
            </list>
        </t>
        <figure title="Field: types of slotframes."                       anchor="tab_types_of_slotframes">
<artwork>
+-----------+---------------------------------+
|    Num.   |           Description           |
+-----------+---------------------------------+
|       0   | Rendez-vous slotframe           |
+-----------+---------------------------------+
|       1   | Receiver-based slotframe        |
+-----------+---------------------------------+
|       2   | Sender-based slotframe          |
+-----------+---------------------------------+
| 128--255  | Reserved                        |
+-----------+---------------------------------+
</artwork>
        </figure>
        <figure title="Field: neighbor set."                              anchor="tab_nbr_set">
<artwork>
+-----------+---------------------------------+
|    Num.   |           Description           |
+-----------+---------------------------------+
|       0   | Empty set                       |
+-----------+---------------------------------+
|       1   | All TSCH time sources           |
+-----------+---------------------------------+
|       2   | All RPL parents                 |
+-----------+---------------------------------+
|       3   | The RPL preferred parent        |
+-----------+---------------------------------+
|       4   | All IPv6 NDP neighbors          |
+-----------+---------------------------------+
| 128--255  | Reserved                        |
+-----------+---------------------------------+
</artwork>
        </figure>
        <figure title="Field: Hash function"                              anchor="tab_hash_func">
<artwork>
+-----------+---------------------------------+
|    Num.   |           Description           |
+-----------+---------------------------------+
|       0   | SAX (Shift-Add-XOR)             |
+-----------+---------------------------------+
|   1--255  | Reserved                        |
+-----------+---------------------------------+
</artwork>
        </figure>
        <figure title="Format of the ASF SIGNAL traffic filters."         anchor="tab_asf_signal_filters">
<artwork>
                  1                   2                   3
0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|   FT  | UC/BC |  IP protocol  |      Type / Code / Port       |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
</artwork>
        </figure>
        <t>
            The fields for traffic filter descriptions (<xref target="tab_asf_signal_filters"/>) are described next:
            <list hangIndent="3" style="hanging">
                <t hangText="FT:">
                    The IEEE 802.15.4 frame type. Examples; 0: Beacon; 1: Data.
                </t>
                <t hangText="UC/BC:">
                    Unicast/broadcast nature. Possible values; 0: unicast; 1: broadcast; 2: any.
                </t>
                <t hangText="IP protocol:">
                    The IP protocol number. Examples; 0x3a: ICMPv6; 0x11: UDP.
                    A value of 0x00 ignores this field.
                </t>
                <t hangText="Type / Code / Port:">
                    In the case of ICMPv6: the Type (first 8 bits) followed by the Code (last 8 bits).
                    In case of UDP or TCP: the 16-bit port.
                    A value of 0x00 ignores this field.
                </t>
            </list>
        </t>
        <t>
            Example filters are given next:
            <list hangIndent="3" style="hanging">
                <t hangText="All TSCH EBs:">
                    FT: 0 (beacon), UC/BC: 1 (broadcast), IP protocol: 0, Type: 0.
                </t>
                <t hangText="All RPL Traffic:">
                    FT: 1 (data), UC/BC: 2 (unicast and broadcast), IP protocol: 0x3a (ICMPv6), Type: 0x9b (RPL), Code: 0x00
                </t>
                <t hangText="RPL Unicast DIO:">
                    FT: 1 (data), UC/BC: 0 (unicast), IP protocol: 0x3a (ICMPv6), Type: 0x9b (RPL), Code: 0x01 (DIO)
                </t>
                <t hangText="UDP port 5683:">
                    FT: 1 (data), UC/BC: 2 (unicast and broadcast), IP protocol: 0x11 (UDP), Port: 5683
                </t>
            </list>
        </t>
    </section>
    <section title="Scheduling Function Identifier"                       anchor="sec_sfid">
        <t>
            The Scheduling Function Identifier (SFID) of ASF is IANA_SFID_ASF.
        </t>
    </section>
    <section title="Rules for Adding/Deleting Cells"                      anchor="sec_scheduling">
        <t>
            ASF nodes maintain their cells autonomously, and do not use 6P ADD nor DELETE.
        </t>
    </section>
    <section title="Rules for CellList"                                   anchor="sec_celllist">
        <t>
            For the 6P LIST command, ASF uses the default CellList field format defined in Section 4.2.4 [TODO: update if needed] of <xref target="I-D.ietf-6tisch-6top-protocol"/>.
        </t>
    </section>
    <section title="6P Timeout Value"                                     anchor="sec_timeout">
        <t>
            The timeout is of low criticality in ASF as 6P Requests are only used for schedule inspection, not for cell addition/removal.
            The RECOMMENDED timeout value in slots is:
        </t>
        <t>
            2^(macMaxBe+2)*SlotframeD_len
        </t>
        <t>
            which is an upper bound of the maximum time spent in transmission attempts of a 6P Request and Response, over slotframeD (where 6P traffic is sent).
            The upper bound is conservative, giving extra time for time spent in packet queues.
        </t>
    </section>
    <section title="Rule for Ordering Cells"                              anchor="sec_ordering">
        <t>
            Cells are ordered by increasing slotframe handle, then by timeslot, then channel offset.
        </t>
    </section>
    <section title="Meaning of the Metadata Field"                        anchor="sec_container">
        <t>
            The Metadata 16-bit field is used as follows:
            <xref target="tab_metadata_field" /> shows the format of the Metadata field, where:
            <list style="symbols">
                <t>
                    Slotframe: is used to identify a slotframe by its handle.
                </t>
                <t>
                    Bits 8-15 are reserved.
                </t>
            </list>
            <figure title="Format of the Metadata Field."                 anchor="tab_metadata_field">
<artwork>
                  1                   2                   3
0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|           Slotframe           |            Reserved           |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
</artwork>
            </figure>
        </t>
    </section>
    <section title="Node Behavior at Boot"                                anchor="sec_boot">
        <t>
            At boot, nodes start with an empty schedule.
            When associating, they configure their schedule with the 6P ASF SIGNAL command, which is included either in the initial EB or later packets, as described in <xref target="sec_configparams" />.
        </t>
    </section>
    <section title="6P Error Handling"                                    anchor="sec_error_handling">
        <t>
            ASF only uses 6P commands COUNT and LIST.
            In case of error on STATUS or LIST, the node MAY retry to contact this neighbor after the 6P timeout.
        </t>
    </section>
    <section title="Examples"                                             anchor="sec_examples">
        <t>
            TODO
        </t>
    </section>
    <section title="[TEMPORARY] Implementation Status"                    anchor="sec_implementation">
        <!-- START BOILERPLATE FROM RFC6982 -->
        <t>
            This section records the status of known implementations of the protocol defined by this specification at the time of posting of this Internet-Draft, and is based on a proposal described in <xref target="RFC6982" />.
            The description of implementations in this section is intended to assist the IETF in its decision processes in progressing drafts to RFCs.
            Please note that the listing of any individual implementation here does not imply endorsement by the IETF.
            Furthermore, no effort has been spent to verify the information presented here that was supplied by IETF contributors.
            This is not intended as, and must not be construed to be, a catalog of available implementations or their features.
            Readers are advised to note that other implementations may exist.
        </t>
        <t>
            According to <xref target="RFC6982" />, "this will allow reviewers and working groups to assign due consideration to documents that have the benefit of running code, which may serve as evidence of valuable experimentation and feedback that have made the implemented protocols more mature.
            It is up to the individual working groups to use this information as they see fit".
        </t>
        <!-- STOP BOILERPLATE FROM RFC6982 -->
        <t>
            <list style="hanging">
                <t hangText="Contiki:">
                    The mechanism behind this specification is implemented in the Contiki project <xref target="Contiki" />.
                    Adjustments to exactly match this specification are in progress.
                    The mechanism was evaluated experimentally in large-scale testbeds in <xref target="Orchestra-SenSys" />.
                </t>
            </list>
        </t>
    </section>
    <section title="Security Considerations"                              anchor="sec_security">
        <t>
            At run-time, ASF is not threatened by attacks on 6P messages as it operates without signaling.
            However, it bases its TSCH schedule on external information, namely:
                (1) the identify of the current TSCH time source and
                (2) the MAC address of its neighbors.
            ASF relies on link-layer security to ensure the integrity of the above information.
        </t>
        <t>
            At configuration time, ASF relies on a 6P SIGNAL command.
            This command MAY be secured as described in <xref target="sec_configparams" />.
            When this command is not secured, the security of the network is equivalent to that of the 6TiSCH minimal configuration (<xref target="RFC8180"/>).
            That is, the network schedule is propagated directly through EBs.
        </t>
    </section>
    <section title="IANA Considerations"                                  anchor="sec_iana">
        <section title="6P Scheduling Function Identifiers 'ASF'"         anchor="sec_iana_sfid_asf">
            <t>
                This document adds the following number to the "6P Scheduling Function Identifiers" registry defined by <xref target="I-D.ietf-6tisch-6top-protocol"/>:
            </t>
            <figure title="6P Scheduling Function Identifiers 'ASF'."     anchor="fig_iana_sfid_asf">
<artwork><![CDATA[
+----------------------+--------------------------------+-----------+
| SFID                 | Name                           | Reference |
+----------------------+--------------------------------+-----------+
| IANA_6TiSCH_SFID_ASF | Autonomous Scheduling Function | RFCXXXX   |
|                      | (ASF)                          |           |
+----------------------+--------------------------------+-----------+
]]></artwork>
            </figure>
        </section>
    </section>
</middle>
<back>
    <references title="Normative References">
        <!-- RFC 6TiSCH-->
        <!-- RFC others -->
        <?rfc include='reference.RFC.2119'?>     <!-- Key words for use in RFCs to Indicate Requirement Levels -->
        <!-- I-D 6TiSCH -->
        <!-- I-D others -->
        <!-- external -->
        <reference                                                        anchor="IEEE802154-2015">
            <front>
                <title>
                    IEEE Std 802.15.4-2015 Standard for Low-Rate Wireless Personal Area Networks (WPANs)
                </title>
                <author>
                    <organization>IEEE standard for Information Technology</organization>
                </author>
                <date month="December" year="2015"/>
            </front>
        </reference>
    </references>
    <references title="Informative References">
       <!-- RFC others -->
        <?rfc include='reference.RFC.8180'?>     <!-- Minimal IPv6 over the TSCH Mode of IEEE 802.15.4e (6TiSCH) Configuration -->
        <?rfc include='reference.RFC.6982'?>     <!-- Improving Awareness of Running Code: The Implementation Status Section -->
        <!-- I-D 6TiSCH -->
        <?rfc include='reference.I-D.ietf-6tisch-terminology'?>
        <?rfc include='reference.I-D.ietf-6tisch-6top-protocol'?>
        <!-- I-D others -->
        <!-- external -->
        <reference                                                        anchor="Contiki">
            <front>
                <title>The Contiki Open Source OS for the Internet of Things</title>
                <author initials="A" surname="Dunkels"     fullname="Adam Dunkels" />
                <author initials="A" surname="Lignan"      fullname="Antonio Lignan" />
                <author initials="B" surname="Thébaudeau"  fullname="Benoît Thébaudeau" />
                <author initials="R" surname="Quattlebaum" fullname="Robert Quattlebaum" />
                <author initials="F" surname="Rosendal"    fullname="Fredrik Rosendal" />
                <author initials="G" surname="Oikonomou"   fullname="George Oikonomou" />
                <author initials="L" surname="Deru"        fullname="Laurent Deru" />
                <author initials="M" surname="Alvira"      fullname="Mariano Alvira" />
                <author initials="N" surname="Tsiftes"     fullname="Nicolas Tsiftes" />
                <author initials="O" surname="Schmidt"     fullname="Oliver Schmidt" />
                <author initials="S" surname="Duquennoy"   fullname="Simon Duquennoy" />
                <date month="November" year="2016" />
            </front>
            <seriesInfo name="https://github.com/contiki-os/contiki" value="" />
        </reference>
        <reference                                                        anchor="Orchestra-SenSys">
            <front>
                <title>Orchestra: Robust Mesh Networks Through Autonomously Scheduled TSCH</title>
                <author initials="S" surname="Duquennoy"   fullname="Simon Duquennoy" />
                <author initials="B" surname="Al Nahas"   fullname="Beshr Al Nahas" />
                <author initials="O" surname="Landsiedel"   fullname="Olaf Landsiedel" />
                <author initials="T" surname="Watteyne"   fullname="Thomas Watteyne" />
                <date month="November" year="2015" />
            </front>
            <seriesInfo name="ACM SenSys 2015" value="" />
        </reference>
        <reference                                                        anchor="SAX-DASFAA">
            <front>
                <title> Performance in Practice of String Hashing Functions</title>
                <author initials="M.V" surname="Ramakrishna"/>
                <author initials="J" surname="Zobel"/>
                <date year="1997" />
            </front>
            <seriesInfo name="DASFAA" value="" />
        </reference>
    </references>
    <section title="Contributors"                                         anchor="sec_contributors">
      <t>
          Beshr Al Nahas (Chalmers University, beshr@chalmers.se) and Olaf Landsiedel (Chalmers University, olafl@chalmers.se) contributed to the design and evaluation of ASF.
      </t>
    </section>
    <section title="Acknowledgments"                                      anchor="sec_acks">
        <t>
            TODO people
        </t>
        <t>
            TODO projects
        </t>
    </section>
    <section title="[TEMPORARY] Changelog">
        <t>
            <list style="symbols">
                <t>draft-duquennoy-6tisch-asf-01
                    <list style="symbols">
                        <t>Defines ASF configuration parameters and procedure;</t>
						<t>Defines packet format to disseminate configurations (6P signal);</t>
						<t>Defines burst mode (conditional cells based on 'frame pending' bit);</t>
						<t>Makes Hash function configurable (SAX remains default).</t>
                    </list>
                </t>
            </list>
            <list style="symbols">
                <t>draft-duquennoy-6tisch-asf-00
                    <list style="symbols">
                        <t>Initial draft.</t>
                    </list>
                </t>
            </list>
        </t>
    </section>
</back>
</rfc>
